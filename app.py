# -*- coding: utf-8 -*-
'''
app.py
~~~~~~
This module implements mobile scanner using an email address as access point
'''

import logging   # logging facility
import os        # misc operating system interfaces
import functools # higher-order functions and operations
import traceback # print or retrieve a stack traceback
import sys       # system-specific parameters and functions

import scanner   # user defined, mobile scanner
import mailbox   # user defined, email manipulation
import secrets   # user defined, credentials

logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

EXCEPTIONS = (scanner.CouldNotRead, scanner.NoEdgesFound)  # scan related exceptions
MAIL_USERNAME = secrets.GMAIL_MOBILE_SCAN.get('username')  # app email username
MAIL_PASSWORD = secrets.GMAIL_MOBILE_SCAN.get('password')  # app email password
NOTIFY = secrets.GMAIL_MOBILE_SCAN_NOTIFY.get('username')  # email address if error
BREAK = '\n --------- NEXT TEXT EXTRACTED --------- \n'    # break between extracted text

def lock_process(file=None, logger=None):
    ''' Decorator to prevent process to run multiple time
    :file: physical file to use as sentinel
    :logger: logger to use
    '''
    def decorator_lock(f):
        @functools.wraps(f)
        def lock(*args, **kwargs):

            _file = f.__name__+'.'+sys._getframe().f_code.co_name
            _file = _file if file is None else file
            _file = os.path.join(scanner.where(), _file) if not os.path.isabs(_file) else _file

            if os.path.isfile(_file):
                if logger:
                    start = datetime.datetime.fromtimestamp(os.path.getmtime(_file))
                    logger.info('Process already running %s (started %s)' % (_file, start))
                return None

            open(_file, 'a').close()

            try:
                return f(*args, **kwargs)
            except Exception as e:
                if logger:
                    logger.error(str(e))
                raise e from None
            finally:
                if os.path.isfile(_file):
                    os.remove(_file)
                else:
                    logger.warning('Lock file unexpectedly deleted %s' % _file)

            return None
        return lock
    return decorator_lock

def fail_once(file=None, to=None, logger=None, username=MAIL_USERNAME, password=MAIL_PASSWORD):
    ''' Decorator to skip execution if function already errored
    :file: physical file to use as sentinel
    :to: email address to notify in case of error
    :logger: logger to use
    :username: email username
    :password: email password
    '''
    def decorator_fail(f):
        @functools.wraps(f)
        def fail(*args, **kwargs):

            _file = f.__name__+'.'+sys._getframe().f_code.co_name
            _file = _file if file is None else file
            _file = os.path.join(scanner.where(), _file) if not os.path.isabs(_file) else _file

            if os.path.isfile(_file):
                if logger:
                    logger.debug('File %s exists. Silencing %s' % (_file, f.__name__))
                return None

            try:
                return f(*args, **kwargs)
            except Exception as e:
                message = 'Silencing %s. %s' % (f.__name__, str(e))
                if logger:
                    logger.error(str(e))
                    logger.info(message)
                open(_file, 'a').close()
                if to:
                    mailbox.send_mail(
                        to=to, subject=messsage
                        , username=username, password=password
                        , body='\n'.join(traceback.format_tb(e.__traceback__)))
                raise e from None

            return None
        return fail
    return decorator_fail

@fail_once(to=NOTIFY, logger=logger_module, username=MAIL_USERNAME, password=MAIL_PASSWORD)
@lock_process(logger=logger_module)
def mobile_scanner(folder='inbox', type='UNSEEN', logger=None, trash=True
              , username=MAIL_USERNAME, password=MAIL_PASSWORD):
    '''
    :folder: inbox folder to read
    :type: email type. `ALL` for all mails, `UNSEEN` for unread, `SEEN` for read
    :logger: logger to use
    :trash: whether to move read emails to trash
    :username: email username
    :password: email password
    '''
    mails = mailbox.read_mail(folder=folder, type=type, username=username, password=password)

    if not mails:
        if logger:
            logger.debug('No new emails')
        return None

    for id, message in mails.items():

        if not message.get('attach'):
            if logger:
                logger.debug('Email %s no attachments' %id)
            subject = ['No attachement']
            subject.append(message.get('Date'))
            if message.get('Subject', ''):
                subject.insert(0, message.get('Subject', ''))
            mailbox.send_mail(
                to=message.get('From')
                , cc=message.get('cc')
                , subject=' - '.join(subject)
                , username=username
                , password=password)
            mailbox.mark_read(id, folder=folder)
            continue

        success, attach, extract = True, [], []
        for image in message.get('attach'):
            try:
                if logger:
                    logger.debug('Scanning %s' %image)
                scan = scanner.scan(image)
                text = scanner.text_tesseract(scanned)
                filename = os.path.splitext(os.path.basename(image))[0]+'.pdf'
                pdf = scanner.save(scanned, filename)
                attach.append(pdf)
                extract.append(text)
            except EXCEPTIONS as e:
                success = False
                message = ' '.join((e.message, os.path.basename(image)))
                if logger:
                    logger.error(str(message))
                text.append(message)

        if logger:
            logger.debug('Sending email %s response' %id)
        subject = ['Scan']
        subject.append('Success' if success else 'Fail')
        subject.append(message.get('Date'))
        if message.get('Subject', ''):
            subject.insert(0, message.get('Subject', ''))

        mailbox.send_mail(
            to=message.get('From')
            , cc=message.get('cc')
            , subject=' - '.join(subject)
            , body=BREAK.join(text)
            , files=attach+message.get('attach'))

        if logger:
            logger.debug('Cleaning and marking as read' %id)
        for image in message.get('attach'):
            if os.path.isfile(image):
                os.remove(image)

        mailbox.mark_read(id, folder=folder)

    if trash:
        mailbox.move_trash(folder=folder, type='SEEN', username=username, password=password)

    return None

if __name__=='__main__':

    format = '%(processName)s - %(asctime)s - %(filename)s - '
    format += '%(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    mobile_scanner(logger=logger)

    logger.info('Execution ends %s' % os.path.realpath(__file__))
