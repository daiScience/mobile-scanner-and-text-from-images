# -*- coding: utf-8 -*-
'''
scanner.py
~~~~~~~~~~
This module implements mobile scanner including text extraction from images
'''

import cv2                # opencv api
import pytesseract        # tesseract api
import os                 # misc operating system interfaces
import inspect            # inspect live objects
import errno              # standard errno system symbols
import numpy              # multi-dimensional arrays and matrices
import matplotlib.pyplot  # base plotting
import PIL                # python imaging library
import sys                # system-specific parameters and functions
import logging            # logging facility

CV2POSTITION = (1500,400) # default position for opencv frame to spawn

logger_module = logging.getLogger(__name__)
logger_module.setLevel(logging.INFO)

def where(depth=1):
    ''' Return location where the code is running '''
    return os.path.dirname(os.path.abspath(inspect.stack()[depth][1])) if depth else None

class CouldNotRead(Exception):
    ''' Helper to manage failing read exceptions '''
    def __init__(self):
        Exception.__init__(self)
        self.message = 'Scan failed: could not read'
        return None

class NoEdgesFound(Exception):
    ''' Helper to manage failing scan '''
    def __init__(self):
        Exception.__init__(self)
        self.message = 'Scan failed: no edge found'
        return None

def read(file):
    ''' Read an image file '''
    try:
        if os.path.isfile(file):
            return cv2.imread(file)
        filename = os.path.join(where(), file)
        if os.path.isfile(filename):
            return cv2.imread(filename)
    except:
        raise CouldNotRead from None
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), file)

def cv2show(image, window_name='Press any key to continue', position=CV2POSTITION):
    ''' Show an image at a given position
    :image: cv2 image object
    :window_name: name of the window
    :position: tuple for the position of top left corner of window
    '''
    cv2.namedWindow(window_name)
    if position:
        cv2.moveWindow(window_name, *position)
    cv2.imshow(window_name, image)
    key = cv2.waitKey()
    if key:
        cv2.destroyAllWindows()
    return None

def resize(image, width=500, return_ratio=False):
    ''' Resize an image to a given width
    :image: cv2 image object
    :width: number of pixel for resized width
    :return_ratio: whether to return the rescaling ratio
    '''
    if width is None:
        return (image.copy(), 1) if return_ratio else image.copy()
    ratio = image.shape[1] / width
    interpolation = cv2.INTER_AREA if ratio > 1 else cv2.INTER_CUBIC
    h, w = int(image.shape[0]/ratio), int(image.shape[1]/ratio)
    resized = cv2.resize(image, (w,h), interpolation=interpolation)
    return (resized, ratio) if return_ratio else resized

def canny(image, color=cv2.COLOR_BGR2GRAY, min_val=10, max_val=200):
    ''' Edge detection in image
    :image: cv2 image object
    :color: cv2 color space
    :min_val: minimum value for hysteresis thresholding
    :max_val: maximum value for hysteresis thresholding
    '''
    gray = cv2.cvtColor(image, color)
    edge = cv2.Canny(gray, min_val, max_val)
    edge = cv2.dilate(edge, None)
    return cv2.erode(edge, None)

def edges(image):
    ''' Biggest four side edge of an image '''
    _, cnts, _= cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cnts = sorted(cnts, key = cv2.contourArea, reverse=True)
    for c in cnts:
        epsilon = 0.1*cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, epsilon, True)
        if len(approx)==4:
            return approx
    return None

def order(points):
    ''' Order points clockwise from top left '''
    result = numpy.zeros((4,2), dtype='float32')
    npoints = numpy.array(points)
    s = npoints.sum(axis=1)
    d = numpy.diff(npoints, axis=1)
    result[0] = npoints[numpy.argmin(s)]
    result[2] = npoints[numpy.argmax(s)]
    result[1] = npoints[numpy.argmin(d)]
    result[3] = npoints[numpy.argmax(d)]
    return result

def zoom(image, points, ordered=True):
    ''' Zoom on part of image using 4 point perspective transform
    :image: cv2 image object
    :points: list of four tuples
    :ordered: whether to consider ordered points
    '''
    pts1 = order(points) if ordered else numpy.array(points)
    h, w, _ = image.shape
    pts2 = order([[0,0], [w,0], [0,h], [w,h]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    return cv2.warpPerspective(image, M, (w, h))

def subplot(position, image, title):
    ''' Helper to visualise scan results using matplotlib '''
    matplotlib.pyplot.subplot(position)
    matplotlib.pyplot.imshow(image)
    matplotlib.pyplot.title(title)
    matplotlib.pyplot.axis('off')
    return None

def log(resized, cannied, contoured, zoomed, filename=None, text1='Original picture'
        , text2='After edge detection', text3='After edge detection', text4='After zoom'):
    ''' Create image log of the scan process
    :resized: original or resized image
    :cannied: image post edge detection
    :contoured: image contour detection
    :zoomed: image after four point transform zoom
    '''
    if filename is None:
        filename = '%s_%s.png' % (os.getpid(), sys._getframe().f_code.co_name)
    if not os.path.isfile(filename):
        filename = os.path.join(where(), filename)
    matplotlib.pyplot.figure()
    subplot(221, resize(resized), text1)
    subplot(222, resize(cannied), text2)
    subplot(223, resize(contoured), text3)
    subplot(224, resize(zoomed), text4)
    matplotlib.pyplot.ioff()
    matplotlib.pyplot.savefig(filename, bbox_inches='tight')
    return filename

def scan(file, scale=500, min_val=10, max_val=200, show=False, logimg=False):
    ''' Scan the document on an image file
    :file: file path to the image
    :scale: number of pixel for resized height or width (whichever the greatest)
    :min_val: minimum value for hysteresis thresholding
    :max_val: maximum value for hysteresis thresholding
    :show: whether to show image after every step
    :logimg: whether to save an file of every image step
    '''
    image = read(file)
    h, w = image.shape[:2]
    width = scale if w>h else w*scale/h
    resized, ratio = resize(image, width=width, return_ratio=True)
    if show:
        cv2show(resized, 'Original picture')
    cannied = canny(resized, min_val=min_val, max_val=max_val)
    if show:
        cv2show(cannied, 'After edge detection')
    edge = edges(cannied)
    if edge is None:
        raise NoEdgesFound
    contoured = resized.copy()
    cv2.drawContours(contoured, [edge], -1, (0,255,0), 2)
    if show:
        cv2show(contoured, 'After contour identification')
    points = [(each[0]*ratio).tolist() for each in edge]
    zoomed = zoom(image, points, ordered=True)
    zoomed_resized = resize(zoomed, width=width)
    if show:
        cv2show(zoomed_resized, 'After zoom')
    if logimg:
        filename = 'log_%s_%s.png' % (os.getpid(), sys._getframe().f_code.co_name)
        filename = log(resized, cannied, contoured, zoomed_resized, filename=filename)
        return zoomed, filename
    return zoomed

def unskew(image):
    ''' Remove skewness in an image '''
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bitwise_not(gray)
    _, threshold = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    coords = numpy.column_stack(numpy.where(threshold > 0))
    print(len(coords), cv2.minAreaRect(coords))
    angle = cv2.minAreaRect(coords)[-1]
    angle = -(90 + angle) if angle<-45 else angle
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    return cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

def text_tesseract_old(image, show=False):
    ''' Extract text from image using Tesseract '''
    grayed = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if show:
        cv2show(resize(grayed))
    _, thresholded = cv2.threshold(grayed, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    if show:
        cv2show(resize(thresholded))
    blured = cv2.medianBlur(thresholded, 3)
    if show:
        cv2show(resize(blured))
    text = pytesseract.image_to_string(blured)
    return text

def ocr(image, show=True, logimg=False):
    ''' Prepare image for optical character recognition '''
    grayed = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    if show:
        cv2show(resize(grayed))
    thresholded = cv2.adaptiveThreshold(grayed, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 61, 10)
    if show:
        cv2show(resize(thresholded))
    kernel = numpy.ones((3, 3), numpy.uint8)
    opening = cv2.morphologyEx(thresholded, cv2.MORPH_OPEN, kernel)
    smooth = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    if show:
        cv2show(resize(smooth))
    if logimg:
        filename = 'log_%s_%s.png' % (os.getpid(), sys._getframe().f_code.co_name)
        text2 = 'After gray scaling'
        text3 = 'After binary scaling'
        text4 = 'After smoothing'
        param = dict(text2=text2, text3=text3, text4=text4, filename=filename)
        filename = log(image, grayed, thresholded, smooth, **param)
        return smooth, filename
    return smooth

def text_tesseract(image, config=('-l eng --oem 1 --psm 4'), show=True, logimg=False):
    ''' Extract text from image using Tesseract '''
    prepared = ocr(image, show=show, logimg=logimg)
    if logimg:
        prepared, log = prepared
    text = pytesseract.image_to_string(prepared, config=config)
    return (text, log) if logimg else text

def save(image, filename=None, return_png=False):
    ''' Save an image as pdf
    :image: cv2 image object
    :filename: path name to save the image to
    :return_png: whether to also return png file
    '''
    png_name = '%s_%s.png' % (os.getpid(), sys._getframe().f_code.co_name)
    png_name = os.path.join(where(), png_name)
    if filename is None:
        filename = '%s_%s.pdf' % (os.getpid(), sys._getframe().f_code.co_name)
    file = os.path.join(where(), filename) if not os.path.isabs(filename) else filename
    try:
        cv2.imwrite(png_name, image)
        PIL.Image.open(png_name).save(file)
        return (file, png_name) if return_png else file
    except Exception as e:
        if os.path.isfile(png_name):
            os.remove(png_name)
        raise e from None
    finally:
        if not return_png and os.path.isfile(png_name):
            os.remove(png_name)

if __name__=='__main__':

    testfile = 'example.jpg'          # example file
    param_scan = dict(show=True       # scan parameter, whether to display processed image
                     , logimg=True)   # scan parameter, whether to save the image log
    return_png = True                 # save parameter, whether to also save a png on top of pdf
    param_ocr = dict(show=True        # ocr parameter, whether to display processed image
                     , logimg=True)   # ocr parameter, whether to save the image log

    format = '%(processName)s - %(asctime)s - %(filename)s - '
    format += '%(funcName)s - %(lineno)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=format)

    logger = logging.getLogger()
    logger.setLevel(logging.INFO)
    logger.info('Execution starts %s' % os.path.realpath(__file__))

    logger.info('Scanning %s' % testfile)
    scanned = scan(testfile, **param_scan)
    if param_scan.get('logimg'):
        scanned, log_scan = scanned

    logger.info('Extracting text from %s' % testfile)
    text = text_tesseract(scanned, **param_ocr)
    if param_ocr.get('logimg'):
        text, log_ocr = text

    logger.info('Saving pdf scan of %s' % testfile)
    pdf = save(scanned, return_png=return_png)
    if return_png:
        pdf, png = pdf

    logger.info('Pdf extracted from %s\n%s' % (testfile, pdf))
    if return_png:
        logger.info('Png extracted from %s\n%s' % (testfile, png))

    if param_scan.get('logimg'):
        logger.info('Log of the scan from %s\n%s' % (testfile, log_scan))

    logger.info('Text extracted from %s\n%s' % (testfile, text))

    if param_ocr.get('logimg'):
        logger.info('Log of the ocr from %s\n%s' % (testfile, log_ocr))

    logger.info('Execution ends %s' % os.path.realpath(__file__))
