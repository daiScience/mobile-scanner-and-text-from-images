# Mobile scanner and text from images

This repository implements a mobile scanner including text extraction from image with email address is used as a request point. The implementation includes 

1. A module to manipulate images, perform edge detection and four point transform as well as extract text from inages 
2. A module to manipulate emails and generally manage a mailbox to be used as an access point 


## Explanation 

This mobile scanner was originally implemented to archive physical letters received and generally any paper documents with any important information, 
with the idea to conveniently search through the content of these letters using basic mailbox search capabilities at a later date. 
Simply send a picture by email and receive a reply containing scanner like zoom pdf image and any text extracted from the image.    

The mobile scanner is implemented as per the below algorithm 

1. For every mail received in a dedicated mailbox, for every attached image
	1. The image is resized to a more manageable width. It speeds the process and remove a lot of noise on high quality images
	2. Edge detection is performed on the image. The biggest four side edge is assumed to be the contour of the main object
	3. A four point perspective transform is perform to zoom on the area of the main object using the original non-resized image
	4. Any text from the zoomed image is extracted using the optical character recognition tool Tesseract
2. An reply email is sent to the original sender containing original picture, zoom pictured pdf and the extracted text 

A different color background tends to improve the performance of the edge detection step a lot. 
An iterative approach on different level of the minimum and maximum values of the Hysteresis thresholding would lead to better results at the expense of speed 
([OpenCV Canny Edge Detection](https://docs.opencv.org/3.1.0/da/d22/tutorial_py_canny.html "OpenCV Canny Edge Detection")).

For my personal usage, email was preferred to request the scan so a simple search through created content easily and conveniently be performed. 
The project could be transferred to an phone application instead

## Scan result example

Below step by step log of the scan process on a supermarket receipt, the actual text results is also displayed further below. 
Far from perfect highlighting the importance of the phisical quality of the receipt paper.
On a clean letter from a bank or hmrc, results are perfectly acceptable   

![](https://bitbucket.org/daiScience/mobile-scanner-and-text-from-images/downloads/9852_log_scan.png)

Text extracted:

```text
WAITROSE
& PARTNERS.

Richmond 188: G20 8948 628%
www.waitrose.con/richmand

Ce
wR SEMI § SKIMMED MILK | 0.59
Reduced ite ap:
WR S'DOUGH CRUMPETS 0.20
Reduced item: __.
R S'DOUGH CRUMPETS © 0.29
VY ROBINSONS APPLE /PEAR 1.55
V PEACH FRUTT&BARLEY 1.95
e« Robinsons2 For £2.90 *« -0. 40
“myWai trose card 
KEKKEKKKKKKEABES
5 items
BALANCE DUE C3.89
Visa Debit
KKKRKEERKEKEKKALEE 1 69. 89
CHANGE oo 0.00

x — WATTROSE, OF FERS aly VE SAVED YOU «
```